﻿namespace MidiRouter
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.OutputMapTree = new System.Windows.Forms.TreeView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.IsActive = new System.Windows.Forms.CheckBox();
            this.OutputMapList = new System.Windows.Forms.CheckedListBox();
            this.checkedListBox2 = new System.Windows.Forms.CheckedListBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.MIDIEcho = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // OutputMapTree
            // 
            this.OutputMapTree.Location = new System.Drawing.Point(12, 25);
            this.OutputMapTree.Name = "OutputMapTree";
            this.OutputMapTree.Size = new System.Drawing.Size(189, 422);
            this.OutputMapTree.TabIndex = 0;
            this.OutputMapTree.BeforeCollapse += new System.Windows.Forms.TreeViewCancelEventHandler(this.OutputMapTree_BeforeCollapse);
            this.OutputMapTree.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.OutputMapTree_AfterSelect);
            this.OutputMapTree.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.OutputMapTree_MouseDoubleClick);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.IsActive);
            this.groupBox1.Controls.Add(this.OutputMapList);
            this.groupBox1.Location = new System.Drawing.Point(207, 22);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 203);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "InputGrouping";
            // 
            // IsActive
            // 
            this.IsActive.AutoSize = true;
            this.IsActive.Location = new System.Drawing.Point(7, 26);
            this.IsActive.Name = "IsActive";
            this.IsActive.Size = new System.Drawing.Size(54, 17);
            this.IsActive.TabIndex = 1;
            this.IsActive.Text = "Listen";
            this.IsActive.UseVisualStyleBackColor = true;
            // 
            // OutputMapList
            // 
            this.OutputMapList.FormattingEnabled = true;
            this.OutputMapList.Location = new System.Drawing.Point(6, 49);
            this.OutputMapList.Name = "OutputMapList";
            this.OutputMapList.Size = new System.Drawing.Size(188, 139);
            this.OutputMapList.TabIndex = 0;
            // 
            // checkedListBox2
            // 
            this.checkedListBox2.FormattingEnabled = true;
            this.checkedListBox2.Location = new System.Drawing.Point(6, 19);
            this.checkedListBox2.Name = "checkedListBox2";
            this.checkedListBox2.Size = new System.Drawing.Size(188, 169);
            this.checkedListBox2.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.checkedListBox2);
            this.groupBox2.Location = new System.Drawing.Point(413, 25);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(200, 200);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "groupBox2";
            // 
            // MIDIEcho
            // 
            this.MIDIEcho.Location = new System.Drawing.Point(6, 19);
            this.MIDIEcho.Multiline = true;
            this.MIDIEcho.Name = "MIDIEcho";
            this.MIDIEcho.ReadOnly = true;
            this.MIDIEcho.Size = new System.Drawing.Size(387, 188);
            this.MIDIEcho.TabIndex = 1;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.MIDIEcho);
            this.groupBox3.Location = new System.Drawing.Point(214, 231);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(393, 216);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Echo";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(629, 459);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.OutputMapTree);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TreeView OutputMapTree;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckedListBox OutputMapList;
        private System.Windows.Forms.CheckedListBox checkedListBox2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox IsActive;
        private System.Windows.Forms.TextBox MIDIEcho;
        private System.Windows.Forms.GroupBox groupBox3;
    }
}

