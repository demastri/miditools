﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Sanford.Multimedia;
using Sanford.Multimedia.Midi;

namespace MidiRouter
{
    public partial class Form1 : Form
    {
        private List<InputDevice> inDevices;
        private List<OutputDevice> outDevices;
        private SynchronizationContext context;

        class MidiMapSet
        {
            public string thisInputName;
            public bool listening;
            public Dictionary<string, bool> routeToOut;

            public MidiMapSet(string s)
            {
                thisInputName = s;
                listening = false;
                routeToOut = new Dictionary<string, bool>();
            }
        }

        bool inInit = true;
        Dictionary<string, MidiMapSet> Mappings;
        string currentInput = "";
        string currentOutput = "";

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            if (InputDevice.DeviceCount == 0)
            {
                MessageBox.Show("No MIDI input devices available.", "Error!",
                    MessageBoxButtons.OK, MessageBoxIcon.Stop);
                Close();
            }
            else
            {
                try
                {
                    context = SynchronizationContext.Current;
                    Mappings = new Dictionary<string, MidiMapSet>();
                    inDevices = new List<InputDevice>();
                    outDevices = new List<OutputDevice>();

                    for (int i = 0; i < InputDevice.DeviceCount; i++)
                    {
                        inDevices.Add( new InputDevice(i) );
                        inDevices[i].ChannelMessageReceived += HandleChannelMessageReceived;
                        inDevices[i].SysCommonMessageReceived += HandleSysCommonMessageReceived;
                        //inDevices[i].SysExMessageReceived += HandleSysExMessageReceived;
                        //inDevices[i].SysRealtimeMessageReceived += HandleSysRealtimeMessageReceived;
                        //inDevices[i].Error += new EventHandler<ErrorEventArgs>(inDevice_Error);
                        
                        string thisName = "In - " + InputDevice.GetDeviceCapabilities(i).name;
                        MidiMapSet thisMap = new MidiMapSet(thisName);
                        TreeNode thisNode = OutputMapTree.Nodes.Add(thisName);
                        bool initOut = outDevices.Count == 0;
                        for (int j = 0; j < OutputDevice.DeviceCount; j++)
                        {
                            if( initOut )
                                outDevices.Add(new OutputDevice(j));

                            string thisOut = "Out - " + OutputDevice.GetDeviceCapabilities(j).name;
                            MidiMapSet thisOutMap = new MidiMapSet(thisOut);
                            thisMap.routeToOut.Add(thisOut, false);

                            thisNode.Nodes.Add(thisOut);
                        }
                        Mappings.Add(thisName, thisMap);
                    }

                    for (int j = 0; j < OutputDevice.DeviceCount; j++)
                    {
                        OutputMapList.Items.Add("Out - " + OutputDevice.GetDeviceCapabilities(j).name, false);
                    }
                    OutputMapTree.ExpandAll();
                    inInit = false;

                    UpdateDisplay();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error!",
                        MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    Close();
                }
            }
        }

        private void OutputMapTree_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (e.Node.Parent == null && Mappings.ContainsKey(e.Node.Text))
            {
                currentInput = e.Node.Text;
                currentOutput = "";
            }
            else
            {
                currentInput = e.Node.Parent.Text;
                currentOutput = e.Node.Text;
            }
            UpdateDisplay();
        }

        private void OutputMapTree_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (currentInput != "" && currentOutput == "")
            {
                Mappings[currentInput].listening = !Mappings[currentInput].listening;
            }
            else if (currentInput != "" && currentOutput != "")
            {
                Mappings[currentInput].routeToOut[currentOutput] = !Mappings[currentInput].routeToOut[currentOutput];
            }
            UpdateDisplay();
        }
        private void UpdateDisplay()
        {
            if (currentInput == "")
            {
                IsActive.Checked = false;
                IsActive.Enabled = false;
            }
            else
            {
                {
                    IsActive.Enabled = true;

                    IsActive.Checked = Mappings[currentInput].listening;
                    for (int i = 0; i < OutputMapList.Items.Count; i++)
                    {
                        OutputMapList.SetItemChecked(i, Mappings[currentInput].routeToOut[OutputMapList.Items[i].ToString()]);
                    }
                }
            }

            foreach (TreeNode n in OutputMapTree.Nodes)
            {
                n.ForeColor = Mappings[n.Text].listening ? Color.Black : Color.LightGray;
                int inIndex = OutputMapTree.Nodes.IndexOf(n);
                if (Mappings[n.Text].listening)
                {
                    inDevices[inIndex].StartRecording();
                }
                else
                {
                    inDevices[inIndex].StopRecording();
                }

                foreach (TreeNode nn in n.Nodes)
                {
                    nn.ForeColor = Mappings[n.Text].listening && Mappings[n.Text].routeToOut[nn.Text] ? Color.Black : Color.LightGray;
                }


            }
        }

        private void OutputMapTree_BeforeCollapse(object sender, TreeViewCancelEventArgs e)
        {
            e.Cancel = !inInit;
        }
        private void HandleChannelMessageReceived(object sender, ChannelMessageEventArgs e)
        {
            outDevices[0].Send(e.Message);
            return;
            context.Post(delegate(object dummy)
            {
                MIDIEcho.AppendText(
                    e.Message.Command.ToString() + '\t' + '\t' +
                    e.Message.MidiChannel.ToString() + '\t' +
                    e.Message.Data1.ToString() + '\t' +
                    e.Message.Data2.ToString() + Environment.NewLine);
            }, null);
        }

        private void HandleSysCommonMessageReceived(object sender, SysCommonMessageEventArgs e)
        {
            context.Post(delegate(object dummy)
            {
                MIDIEcho.AppendText(
                    e.Message.SysCommonType.ToString() + '\t' + '\t' +
                    e.Message.Data1.ToString() + '\t' +
                    e.Message.Data2.ToString() + Environment.NewLine );

            }, null);
        }
    }
}
